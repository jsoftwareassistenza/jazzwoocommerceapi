/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jazzwcapi.wordpress.test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.List;
import jazzwcapi.wordpress.FilterPost;
import jazzwcapi.wordpress.MediaItemUploadResult;
import jazzwcapi.wordpress.Post;
import jazzwcapi.wordpress.Wordpress;
import jazzwcapi.wordpress.exceptions.FileUploadException;
import jazzwcapi.wordpress.exceptions.InsufficientRightsException;
import jazzwcapi.wordpress.exceptions.InvalidArgumentsException;
import jazzwcapi.wordpress.exceptions.ObjectNotFoundException;

import redstone.xmlrpc.XmlRpcFault;
//import net.sf.json.JSONArray;

/**
 *
 * @author Giovanni
 */
public class WcRestWordpress {

    public WcRestWordpress() {
    }

    public static void main(String args[]) throws XmlRpcFault, InsufficientRightsException, InvalidArgumentsException, ObjectNotFoundException, MalformedURLException, FileNotFoundException, FileUploadException, IOException {
        Wordpress wp = new Wordpress("jsoftware", "triangolino", "http://192.168.10.201:81/wordpress/xmlrpc.php");
        FilterPost filter = new FilterPost();
        filter.setNumber(10);
        List<Post> recentPosts = wp.getPosts(filter);
        System.out.println("Here are the ten recent posts:");
        for (Post page : recentPosts) {
            System.out.println(page.getPost_id() + ":" + page.getPost_title());
        }
        FilterPost filter2 = new FilterPost();
        filter2.setPost_type("page");
        wp.getPosts(filter2);
        List<Post> pages = wp.getPosts(filter2);
        System.out.println("Here are the pages:");
        for (Post pageDefinition : pages) {
            System.out.println(pageDefinition.getPost_title());
        }
        System.out.println("Posting a test (draft) page...");
        Post recentPost = new Post();
        recentPost.setPost_title("Test Page");
        recentPost.setPost_content("Test description");
        recentPost.setPost_status("draft");
        Integer result = wp.newPost(recentPost);
        System.out.println("new post page id: " + result);
        InputStream media = new FileInputStream("C:\\Users\\Giovanni\\Desktop\\rossa2.jpg");
        MediaItemUploadResult mu = wp.uploadFile(media, "Nartigana_rossa2.jpeg");
        System.out.println("Fatto");
    }
}
