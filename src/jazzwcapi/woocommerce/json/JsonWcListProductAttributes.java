/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jazzwcapi.woocommerce.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;

/**
 *
 * @author ago
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonWcListProductAttributes {

    private ArrayList<JsonWcAttribute> l;

    public JsonWcListProductAttributes() {

    }

    public JsonWcListProductAttributes(ArrayList<JsonWcAttribute> l) {
        this.l = l;
    }

    public ArrayList<JsonWcAttribute> getL() {
        return l;
    }

    public void setL(ArrayList<JsonWcAttribute> l) {
        this.l = l;
    }

}
