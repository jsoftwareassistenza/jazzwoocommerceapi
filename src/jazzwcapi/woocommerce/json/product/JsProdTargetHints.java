
package jazzwcapi.woocommerce.json.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import java.util.ArrayList;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "allow"
})
public class JsProdTargetHints {

    @JsonProperty("allow")
    private  ArrayList<String> allow;

    @JsonProperty("allow")
    public ArrayList<String> getAllow() {
        return allow;
    }

    @JsonProperty("allow")
    public void setAllow(ArrayList<String> allow) {
        this.allow = allow;
    }

}
