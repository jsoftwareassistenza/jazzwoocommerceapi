
package jazzwcapi.woocommerce.json.product;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "href",
    "targetHints"
})
public class JsProdSelf {

    @JsonProperty("href")
    private String href;
    @JsonProperty("targetHints")
    private JsProdTargetHints targetHints;

    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

    @JsonProperty("targetHints")
    public JsProdTargetHints getTargetHints() {
        return targetHints;
    }

    @JsonProperty("targetHints")
    public void setTargetHints(JsProdTargetHints targetHints) {
        this.targetHints = targetHints;
    }
}
