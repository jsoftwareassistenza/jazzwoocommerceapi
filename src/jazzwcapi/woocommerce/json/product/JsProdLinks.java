
package jazzwcapi.woocommerce.json.product;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "self",
    "collection"
})
public class JsProdLinks {

    @JsonProperty("self")
    private List<JsProdSelf> self;
    @JsonProperty("collection")
    private List<JsProdCollection> collection;

    @JsonProperty("self")
    public List<JsProdSelf> getSelf() {
        return self;
    }

    @JsonProperty("self")
    public void setSelf(List<JsProdSelf> self) {
        this.self = self;
    }

    @JsonProperty("collection")
    public List<JsProdCollection> getCollection() {
        return collection;
    }

    @JsonProperty("collection")
    public void setCollection(List<JsProdCollection> collection) {
        this.collection = collection;
    }

}
