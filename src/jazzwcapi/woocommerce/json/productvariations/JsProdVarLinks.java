
package jazzwcapi.woocommerce.json.productvariations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "self",
    "collection",
    "up"
})
public class JsProdVarLinks {

    @JsonProperty("self")
    private List<JsProdVarSelf> self = null;
    @JsonProperty("collection")
    private List<JsProdVarCollection> collection = null;
    @JsonProperty("up")
    private List<JsProdVarUp> up = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("self")
    public List<JsProdVarSelf> getSelf() {
        return self;
    }

    @JsonProperty("self")
    public void setSelf(List<JsProdVarSelf> self) {
        this.self = self;
    }

    @JsonProperty("collection")
    public List<JsProdVarCollection> getCollection() {
        return collection;
    }

    @JsonProperty("collection")
    public void setCollection(List<JsProdVarCollection> collection) {
        this.collection = collection;
    }

    @JsonProperty("up")
    public List<JsProdVarUp> getUp() {
        return up;
    }

    @JsonProperty("up")
    public void setUp(List<JsProdVarUp> up) {
        this.up = up;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
