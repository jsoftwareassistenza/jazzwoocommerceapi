
package jazzwcapi.woocommerce.json.productvariations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
    "id",
    "date_created",
    "date_created_gmt",
    "date_modified",
    "date_modified_gmt",
    "description",
    "permalink",
    "sku",
    "price",
    "regular_price",
    "sale_price",
    "date_on_sale_from",
    "date_on_sale_from_gmt",
    "date_on_sale_to",
    "date_on_sale_to_gmt",
    "on_sale",
    "status",
    "purchasable",
    "virtual",
    "downloadable",
    "downloads",
    "download_limit",
    "download_expiry",
    "tax_status",
    "tax_class",
    "manage_stock",
    "stock_quantity",
    "stock_status",
    "backorders",
    "backorders_allowed",
    "backordered",
    "weight",
    "dimensions",
    "shipping_class",
    "shipping_class_id",
    "image",
    "attributes",
    "menu_order",
    "_links"
})
public class JsProdVarProduct {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("date_created")
    private String dateCreated;
    @JsonProperty("date_created_gmt")
    private String dateCreatedGmt;
    @JsonProperty("date_modified")
    private String dateModified;
    @JsonProperty("date_modified_gmt")
    private String dateModifiedGmt;
    @JsonProperty("description")
    private String description;
    @JsonProperty("permalink")
    private String permalink;
    @JsonProperty("sku")
    private String sku;
    @JsonProperty("price")
    private String price;
    @JsonProperty("regular_price")
    private String regularPrice;
    @JsonProperty("sale_price")
    private String salePrice;
    @JsonProperty("date_on_sale_from")
    private Object dateOnSaleFrom;
    @JsonProperty("date_on_sale_from_gmt")
    private Object dateOnSaleFromGmt;
    @JsonProperty("date_on_sale_to")
    private Object dateOnSaleTo;
    @JsonProperty("date_on_sale_to_gmt")
    private Object dateOnSaleToGmt;
    @JsonProperty("on_sale")
    private Boolean onSale;
    @JsonProperty("status")
    private String status;
    @JsonProperty("purchasable")
    private Boolean purchasable;
    @JsonProperty("virtual")
    private Boolean virtual;
    @JsonProperty("downloadable")
    private Boolean downloadable;
    @JsonProperty("downloads")
    private List<JsProdVarDownload> downloads = null;
    @JsonProperty("download_limit")
    private Integer downloadLimit;
    @JsonProperty("download_expiry")
    private Integer downloadExpiry;
    @JsonProperty("tax_status")
    private String taxStatus;
    @JsonProperty("tax_class")
    private String taxClass;
    @JsonProperty("manage_stock")
    private Boolean manageStock;
    @JsonProperty("stock_quantity")
    private Object stockQuantity;
    @JsonProperty("stock_status")
    private String stockStatus;
    @JsonProperty("backorders")
    private String backorders;
    @JsonProperty("backorders_allowed")
    private Boolean backordersAllowed;
    @JsonProperty("backordered")
    private Boolean backordered;
    @JsonProperty("weight")
    private String weight;
    @JsonProperty("dimensions")
    private JsProdVarDimensions dimensions;
    @JsonProperty("shipping_class")
    private String shippingClass;
    @JsonProperty("shipping_class_id")
    private Integer shippingClassId;
    @JsonProperty("image")
    private JsProdVarImage image;
    @JsonProperty("attributes")
    private List<JsProdVarAttribute> attributes = null;
    @JsonProperty("menu_order")
    private Integer menuOrder;
//    @JsonProperty("meta_data")
//    private List<JsProdVarMetaDatum> metaData = null;
    @JsonProperty("_links")
    private JsProdVarLinks links;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("date_created")
    public String getDateCreated() {
        return dateCreated;
    }

    @JsonProperty("date_created")
    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    @JsonProperty("date_created_gmt")
    public String getDateCreatedGmt() {
        return dateCreatedGmt;
    }

    @JsonProperty("date_created_gmt")
    public void setDateCreatedGmt(String dateCreatedGmt) {
        this.dateCreatedGmt = dateCreatedGmt;
    }

    @JsonProperty("date_modified")
    public String getDateModified() {
        return dateModified;
    }

    @JsonProperty("date_modified")
    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    @JsonProperty("date_modified_gmt")
    public String getDateModifiedGmt() {
        return dateModifiedGmt;
    }

    @JsonProperty("date_modified_gmt")
    public void setDateModifiedGmt(String dateModifiedGmt) {
        this.dateModifiedGmt = dateModifiedGmt;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("permalink")
    public String getPermalink() {
        return permalink;
    }

    @JsonProperty("permalink")
    public void setPermalink(String permalink) {
        this.permalink = permalink;
    }

    @JsonProperty("sku")
    public String getSku() {
        return sku;
    }

    @JsonProperty("sku")
    public void setSku(String sku) {
        this.sku = sku;
    }

    @JsonProperty("price")
    public String getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(String price) {
        this.price = price;
    }

    @JsonProperty("regular_price")
    public String getRegularPrice() {
        return regularPrice;
    }

    @JsonProperty("regular_price")
    public void setRegularPrice(String regularPrice) {
        this.regularPrice = regularPrice;
    }

    @JsonProperty("sale_price")
    public String getSalePrice() {
        return salePrice;
    }

    @JsonProperty("sale_price")
    public void setSalePrice(String salePrice) {
        this.salePrice = salePrice;
    }

    @JsonProperty("date_on_sale_from")
    public Object getDateOnSaleFrom() {
        return dateOnSaleFrom;
    }

    @JsonProperty("date_on_sale_from")
    public void setDateOnSaleFrom(Object dateOnSaleFrom) {
        this.dateOnSaleFrom = dateOnSaleFrom;
    }

    @JsonProperty("date_on_sale_from_gmt")
    public Object getDateOnSaleFromGmt() {
        return dateOnSaleFromGmt;
    }

    @JsonProperty("date_on_sale_from_gmt")
    public void setDateOnSaleFromGmt(Object dateOnSaleFromGmt) {
        this.dateOnSaleFromGmt = dateOnSaleFromGmt;
    }

    @JsonProperty("date_on_sale_to")
    public Object getDateOnSaleTo() {
        return dateOnSaleTo;
    }

    @JsonProperty("date_on_sale_to")
    public void setDateOnSaleTo(Object dateOnSaleTo) {
        this.dateOnSaleTo = dateOnSaleTo;
    }

    @JsonProperty("date_on_sale_to_gmt")
    public Object getDateOnSaleToGmt() {
        return dateOnSaleToGmt;
    }

    @JsonProperty("date_on_sale_to_gmt")
    public void setDateOnSaleToGmt(Object dateOnSaleToGmt) {
        this.dateOnSaleToGmt = dateOnSaleToGmt;
    }

    @JsonProperty("on_sale")
    public Boolean getOnSale() {
        return onSale;
    }

    @JsonProperty("on_sale")
    public void setOnSale(Boolean onSale) {
        this.onSale = onSale;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("purchasable")
    public Boolean getPurchasable() {
        return purchasable;
    }

    @JsonProperty("purchasable")
    public void setPurchasable(Boolean purchasable) {
        this.purchasable = purchasable;
    }

    @JsonProperty("virtual")
    public Boolean getVirtual() {
        return virtual;
    }

    @JsonProperty("virtual")
    public void setVirtual(Boolean virtual) {
        this.virtual = virtual;
    }

    @JsonProperty("downloadable")
    public Boolean getDownloadable() {
        return downloadable;
    }

    @JsonProperty("downloadable")
    public void setDownloadable(Boolean downloadable) {
        this.downloadable = downloadable;
    }

    @JsonProperty("downloads")
    public List<JsProdVarDownload> getDownloads() {
        return downloads;
    }

    @JsonProperty("downloads")
    public void setDownloads(List<JsProdVarDownload> downloads) {
        this.downloads = downloads;
    }

    @JsonProperty("download_limit")
    public Integer getDownloadLimit() {
        return downloadLimit;
    }

    @JsonProperty("download_limit")
    public void setDownloadLimit(Integer downloadLimit) {
        this.downloadLimit = downloadLimit;
    }

    @JsonProperty("download_expiry")
    public Integer getDownloadExpiry() {
        return downloadExpiry;
    }

    @JsonProperty("download_expiry")
    public void setDownloadExpiry(Integer downloadExpiry) {
        this.downloadExpiry = downloadExpiry;
    }

    @JsonProperty("tax_status")
    public String getTaxStatus() {
        return taxStatus;
    }

    @JsonProperty("tax_status")
    public void setTaxStatus(String taxStatus) {
        this.taxStatus = taxStatus;
    }

    @JsonProperty("tax_class")
    public String getTaxClass() {
        return taxClass;
    }

    @JsonProperty("tax_class")
    public void setTaxClass(String taxClass) {
        this.taxClass = taxClass;
    }

    @JsonProperty("manage_stock")
    public Boolean getManageStock() {
        return manageStock;
    }

    @JsonProperty("manage_stock")
    public void setManageStock(Boolean manageStock) {
        this.manageStock = manageStock;
    }

    @JsonProperty("stock_quantity")
    public Object getStockQuantity() {
        return stockQuantity;
    }

    @JsonProperty("stock_quantity")
    public void setStockQuantity(Object stockQuantity) {
        this.stockQuantity = stockQuantity;
    }

    @JsonProperty("stock_status")
    public String getStockStatus() {
        return stockStatus;
    }

    @JsonProperty("stock_status")
    public void setStockStatus(String stockStatus) {
        this.stockStatus = stockStatus;
    }

    @JsonProperty("backorders")
    public String getBackorders() {
        return backorders;
    }

    @JsonProperty("backorders")
    public void setBackorders(String backorders) {
        this.backorders = backorders;
    }

    @JsonProperty("backorders_allowed")
    public Boolean getBackordersAllowed() {
        return backordersAllowed;
    }

    @JsonProperty("backorders_allowed")
    public void setBackordersAllowed(Boolean backordersAllowed) {
        this.backordersAllowed = backordersAllowed;
    }

    @JsonProperty("backordered")
    public Boolean getBackordered() {
        return backordered;
    }

    @JsonProperty("backordered")
    public void setBackordered(Boolean backordered) {
        this.backordered = backordered;
    }

    @JsonProperty("weight")
    public String getWeight() {
        return weight;
    }

    @JsonProperty("weight")
    public void setWeight(String weight) {
        this.weight = weight;
    }

    @JsonProperty("dimensions")
    public JsProdVarDimensions getDimensions() {
        return dimensions;
    }

    @JsonProperty("dimensions")
    public void setDimensions(JsProdVarDimensions dimensions) {
        this.dimensions = dimensions;
    }

    @JsonProperty("shipping_class")
    public String getShippingClass() {
        return shippingClass;
    }

    @JsonProperty("shipping_class")
    public void setShippingClass(String shippingClass) {
        this.shippingClass = shippingClass;
    }

    @JsonProperty("shipping_class_id")
    public Integer getShippingClassId() {
        return shippingClassId;
    }

    @JsonProperty("shipping_class_id")
    public void setShippingClassId(Integer shippingClassId) {
        this.shippingClassId = shippingClassId;
    }

    @JsonProperty("image")
    public JsProdVarImage getImage() {
        return image;
    }

    @JsonProperty("image")
    public void setImage(JsProdVarImage image) {
        this.image = image;
    }

    @JsonProperty("attributes")
    public List<JsProdVarAttribute> getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(List<JsProdVarAttribute> attributes) {
        this.attributes = attributes;
    }

    @JsonProperty("menu_order")
    public Integer getMenuOrder() {
        return menuOrder;
    }

    @JsonProperty("menu_order")
    public void setMenuOrder(Integer menuOrder) {
        this.menuOrder = menuOrder;
    }

//    @JsonProperty("meta_data")
//    public List<JsProdVarMetaDatum> getMetaData() {
//        return metaData;
//    }
//
//    @JsonProperty("meta_data")
//    public void setMetaData(List<JsProdVarMetaDatum> metaData) {
//        this.metaData = metaData;
//    }

    @JsonProperty("_links")
    public JsProdVarLinks getLinks() {
        return links;
    }

    @JsonProperty("_links")
    public void setLinks(JsProdVarLinks links) {
        this.links = links;
    }
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
