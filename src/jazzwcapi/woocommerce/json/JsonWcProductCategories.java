/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jazzwcapi.woocommerce.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.List;

/**
 *
 * @author ago
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonWcProductCategories {

    private int id;
    private String name;
    private int parent;
//    private List<JsonWcItem>;

    

    public JsonWcProductCategories() {

    }

    public JsonWcProductCategories(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getParent() {
        return parent;
    }

    public void setParent(int parent) {
        this.parent = parent;
    }
}
