
package jazzwcapi.woocommerce.json.order;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "self",
    "collection",
    "customer",
    "up"
})
@Generated("jsonschema2pojo")
public class Links {

    @JsonProperty("self")
    private List<Object> self = null;
    @JsonProperty("collection")
    private List<Object> collection = null;
    @JsonProperty("customer")
    private List<Object> customer = null;
    @JsonProperty("up")
    private List<Object> up = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("self")
    public List<Object> getSelf() {
        return self;
    }

    @JsonProperty("self")
    public void setSelf(List<Object> self) {
        this.self = self;
    }

    @JsonProperty("collection")
    public List<Object> getCollection() {
        return collection;
    }

    @JsonProperty("collection")
    public void setCollection(List<Object> collection) {
        this.collection = collection;
    }

    @JsonProperty("customer")
    public List<Object> getCustomer() {
        return customer;
    }

    @JsonProperty("customer")
    public void setCustomer(List<Object> customer) {
        this.customer = customer;
    }

    @JsonProperty("up")
    public List<Object> getUp() {
        return up;
    }

    @JsonProperty("up")
    public void setUp(List<Object> up) {
        this.up = up;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
