
package jazzwcapi.woocommerce.json.order;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "mode",
    "type",
    "commission_val",
    "commission_fixed"
})
@Generated("jsonschema2pojo")
public class Value {

    @JsonProperty("mode")
    private String mode;
    @JsonProperty("type")
    private String type;
    @JsonProperty("commission_val")
    private Double commissionVal;
    @JsonProperty("commission_fixed")
    private Integer commissionFixed;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("mode")
    public String getMode() {
        return mode;
    }

    @JsonProperty("mode")
    public void setMode(String mode) {
        this.mode = mode;
    }

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("commission_val")
    public Double getCommissionVal() {
        return commissionVal;
    }

    @JsonProperty("commission_val")
    public void setCommissionVal(Double commissionVal) {
        this.commissionVal = commissionVal;
    }

    @JsonProperty("commission_fixed")
    public Integer getCommissionFixed() {
        return commissionFixed;
    }

    @JsonProperty("commission_fixed")
    public void setCommissionFixed(Integer commissionFixed) {
        this.commissionFixed = commissionFixed;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
