/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jazzwcapi.woocommerce.test;

import java.util.ArrayList;
import jazzwcapi.woocommerce.oauth.OAuthConfig;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import jazzwcapi.woocommerce.ApiVersionType;
import jazzwcapi.woocommerce.EndpointBaseType;
import jazzwcapi.woocommerce.WooCommerce;
import jazzwcapi.woocommerce.WooCommerceAPIMod;
import jazzwcapi.woocommerce.json.JsonWcAttribute;
import jazzwcapi.woocommerce.json.JsonWcProductCategories;
import jazzwcapi.woocommerce.json.order.Order;
import jazzwcapi.woocommerce.json.product.JsDeleteBulk;
import jazzwcapi.woocommerce.json.product.JsProdProduct;
//import net.sf.json.JSONArray;

/**
 *
 * @author Giovanni
 */
public class WcRest {

    public WcRest() {
    }

    public static void main(String args[]) {

        // Setup client
//        OAuthConfig config = new OAuthConfig("https://beerple.it", "ck_1c53296f9009921fb9574d52d26414a4dc4b94a7", "cs_a2cc8979e7479ba490d9138df458e72b1e2321cc");
//        OAuthConfig config = new OAuthConfig("http://192.168.10.201:81/wordpress", "ck_6c49dcbe2c32c5a3a8eef0aec5d7173cebfd66d5", "cs_ff1c7ae3e8de8a256d655bf0cb15594908ac773c");
//        OAuthConfig config = new OAuthConfig("https://birrasalentoshop.ioslab.it", "ck_b58158761a1892566809b68bcaafbdcf631254db", "cs_8ea5412fd837b0542b67e8e0a70133c67d0ac7a2");
//        OAuthConfig config = new OAuthConfig("https://www.ilgaggiolino.it", "ck_547c659ea7172a01077bc79dd3f77d5241253ceb", "cs_f5a4506087f200e2132cc0999e0f61eaf0a56622");
        OAuthConfig config = new OAuthConfig("https://www.erboristeriasalute.com/wp", "ck_0d8fd809222b268372085025063c3d3ae65c01e9", "cs_6e4e34e8d4763cbdba51c54147b2f5c74623796b");
//        OAuthConfig config = new OAuthConfig("https://hammer.bitnet.it", "ck_0e30679552ba322a768c04880ea3cb6e934105a8", "cs_73c9bbe346ffd1f4a4db9baff4c65799d7156e79");
        WooCommerce wooCommerceMod = new WooCommerceAPIMod(config, ApiVersionType.V3);

        // Prepare object for request
        Map<String, Object> productInfo = new HashMap<>();
//        productInfo.put("name", "Premiumà Quality");
//        productInfo.put("type", "simple");
//        productInfo.put("regular_price", "21.99");
        productInfo.put("name", "òèFRE5§");

//        Map<String, Object> productAttr = new HashMap<>();
//        productAttr.put("name", "òèFRE5§");
//        wooCommerceMod.create(EndpointBaseType.PRODUCTS_ATTRIBUTES.getValue(), productAttr);
//        wooCommerceMod.create(EndpointBaseType.PRODUCTS.getValue(), productInfo);
//        wooCommerceMod.update(EndpointBaseType.PRODUCTS.getValue(), 2443, productInfo);
        // Make request and retrieve result
//        Map product = wooCommerce.create(EndpointBaseType.PRODUCTS.getValue(), productInfo);
//        System.out.println(product.get("id"));
        // Get all with request parameters
        Map<String, String> params = new HashMap<>();
//        params.put("per_page","100");
//        params.put("page","3");
//        params.put("per_page","100");
//        params.put("offset","0");
//        List products = wooCommerce.getAll(EndpointBaseType.ORDERS.getValue(), params);

//        Customer list = wooCommerceMod.getCustomer(EndpointBaseType.CUSTOMERS.getValue(), params, "30");
//        List list = wooCommerceMod.getAllCustomers(EndpointBaseType.CUSTOMERS.getValue(), params);
        Order list = wooCommerceMod.getOrder(EndpointBaseType.ORDERS.getValue(), params, "13168");
//        List list = wooCommerceMod.getAllOrders(EndpointBaseType.ORDERS.getValue(), params, "", "");
//        List listc = wooCommerceMod.getAllCustomers(EndpointBaseType.CUSTOMERS.getValue(), params);
//        List list = wooCommerceMod.getAllProducts(EndpointBaseType.PRODUCTS.getValue(), params);
//         JsProdProduct pr = wooCommerceMod.getProduct(EndpointBaseType.PRODUCTS.getValue(), 277);
//        List listv = wooCommerceMod.getAllProductsVariations(EndpointBaseType.PRODUCTS.getValue(), params, "188");
//        List list = wooCommerceMod.getAllAttributes(EndpointBaseType.PRODUCTS_ATTRIBUTES.getValue(), params);
//        List list2 = wooCommerceMod.getAllTermsAttribute(EndpointBaseType.PRODUCTS_ATTRIBUTES.getValue(), params, "13");
//        List list = wooCommerceMod.getAllProductCategories(EndpointBaseType.PRODUCTS_CATEGORIES.getValue(), params);
//        List list = wooCommerceMod.getAllProducts(EndpointBaseType.PRODUCTS.getValue(), params);
//        List<Order> l  = wooCommerceMod.getAllOrders(EndpointBaseType.ORDERS.getValue(), params, "", "");
        JsProdProduct p = new JsProdProduct();
//        p.setId(274);
        p.setManageStock(true);
        p.setStockQuantity(185);
//        p.setName("§ç°èèçòàì");
//        JsProdImage i = new JsProdImage();
//        i.setId(35);
//        JsProdImage i2 = new JsProdImage();
//        i2.setId(38);
//        List<JsProdImage> l = new ArrayList();
//        l.add(i);
//        l.add(i2);
//        p.setImages(l);
//        wooCommerceMod.updateJson(EndpointBaseType.PRODUCTS.getValue(), 274, p);
//        List<Integer> lid = new ArrayList();
//        List<JsProdProduct> list2 = wooCommerceMod.getAllProducts(EndpointBaseType.PRODUCTS.getValue(), params);
//        for (int i = 0; i < list2.size() - 1; i++) {
//            JsProdProduct pr = list2.get(i);
//            lid.add(pr.getId());
//        }
//        JsDeleteBulk d = new JsDeleteBulk();
//        d.setDelete(lid);
//        wooCommerceMod.batchJson(EndpointBaseType.PRODUCTS.getValue(), d);
//        eliminaAttributiSuSito();
        //        eliminaCategorieSuSito();
        //        wooCommerceMod.getProduct(EndpointBaseType.PRODUCTS.getValue(), 2296);
        //        Map m = wooCommerceMod.updateProduct(EndpointBaseType.PRODUCTS.getValue(), 9, p);
        System.out.println("");
//        eliminaArticoliSuSito();
//        eliminaAttributiSuSito();
//        eliminaCategorieSuSito();

        // Make request and retrieve result
//        Map product = wooCommerce.create(EndpointBaseType.PRODUCTS.getValue(), productInfo);
//        System.out.println(product.get("id"));
        // Get all with request parameters
//        System.out.println(products.size());
    }

    public static void eliminaAttributiSuSito() {
        OAuthConfig config = new OAuthConfig("http://192.168.10.201:81/wordpress", "ck_6c49dcbe2c32c5a3a8eef0aec5d7173cebfd66d5", "cs_ff1c7ae3e8de8a256d655bf0cb15594908ac773c");
        WooCommerce wooCommerce = new WooCommerceAPIMod(config, ApiVersionType.V3);
        Map<String, String> params = new HashMap();

        List<Integer> lid = new ArrayList();
        List<JsonWcAttribute> list = wooCommerce.getAllAttributes(EndpointBaseType.PRODUCTS_ATTRIBUTES.getValue(), params);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                JsonWcAttribute pr = list.get(i);
                lid.add(pr.getId());
            }
            JsDeleteBulk d = new JsDeleteBulk();
            d.setDelete(lid);
            wooCommerce.batchJson(EndpointBaseType.PRODUCTS_ATTRIBUTES.getValue(), d);
        }
//        for (int i = 0; i < list.size(); i++) {
//            JsonWcAttribute a = list.get(i);
//            wooCommerce.delete(EndpointBaseType.PRODUCTS_ATTRIBUTES.getValue(), a.getId());
//        }
    }

    public static void eliminaCategorieSuSito() {
        OAuthConfig config = new OAuthConfig("http://192.168.10.201:81/wordpress", "ck_6c49dcbe2c32c5a3a8eef0aec5d7173cebfd66d5", "cs_ff1c7ae3e8de8a256d655bf0cb15594908ac773c");
        WooCommerce wooCommerce = new WooCommerceAPIMod(config, ApiVersionType.V3);
        Map<String, String> params = new HashMap();

        List<Integer> lid = new ArrayList();
        List<JsonWcProductCategories> list = wooCommerce.getAllProductCategories(EndpointBaseType.PRODUCTS_CATEGORIES.getValue(), params);
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                JsonWcProductCategories pr = list.get(i);
                lid.add(pr.getId());
            }
            JsDeleteBulk d = new JsDeleteBulk();
            d.setDelete(lid);
            wooCommerce.batchJson(EndpointBaseType.PRODUCTS_CATEGORIES.getValue(), d);
        }
//        for (int i = 0; i < list.size(); i++) {
//            JsonWcProductCategories a = list.get(i);
//            wooCommerce.delete(EndpointBaseType.PRODUCTS_CATEGORIES.getValue(), a.getId());
//        }
    }

    public static void eliminaArticoliSuSito() {
        OAuthConfig config = new OAuthConfig("http://192.168.10.201:81/wordpress", "ck_6c49dcbe2c32c5a3a8eef0aec5d7173cebfd66d5", "cs_ff1c7ae3e8de8a256d655bf0cb15594908ac773c");
        WooCommerce wooCommerce = new WooCommerceAPIMod(config, ApiVersionType.V3);
        Map<String, String> params = new HashMap();

        List<Integer> lid = new ArrayList();
        List<JsProdProduct> list = wooCommerce.getAllProducts(EndpointBaseType.PRODUCTS.getValue(), params, "");
        if (list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                JsProdProduct pr = list.get(i);
                lid.add(pr.getId());
            }
            JsDeleteBulk d = new JsDeleteBulk();
            d.setDelete(lid);
            wooCommerce.batchJson(EndpointBaseType.PRODUCTS.getValue(), d);
        }
//        for (int i = 0; i < list.size(); i++) {
//            JsProdProduct pc = (JsProdProduct) list.get(i);
//            wooCommerce.delete(EndpointBaseType.PRODUCTS.getValue(), pc.getId());
//        }
    }
}
