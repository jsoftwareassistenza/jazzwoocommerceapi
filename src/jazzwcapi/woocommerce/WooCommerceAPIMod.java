package jazzwcapi.woocommerce;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import jazzwcapi.woocommerce.oauth.OAuthSignature;
import jazzwcapi.woocommerce.oauth.OAuthConfig;
import java.util.ArrayList;

import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import jazzwcapi.woocommerce.json.JsonWcAttribute;
import jazzwcapi.woocommerce.json.JsonWcAttributeTerms;
import jazzwcapi.woocommerce.json.JsonWcProductCategories;
import jazzwcapi.woocommerce.json.customer.Customer;
import jazzwcapi.woocommerce.json.order.Order;
import jazzwcapi.woocommerce.json.product.JsProdProduct;
import jazzwcapi.woocommerce.json.productvariations.JsProdVarProduct;

public class WooCommerceAPIMod implements WooCommerce {

    private static final String API_URL_FORMAT = "%s/wp-json/wc/%s/%s";
    private static final String API_URL_BATCH_FORMAT = "%s/wp-json/wc/%s/%s/batch";
    private static final String API_URL_ONE_ENTITY_FORMAT = "%s/wp-json/wc/%s/%s/%d";
    private static final String URL_SECURED_FORMAT = "%s?%s";

    private DefaultHttpClient client;
    private OAuthConfig config;
    private String apiVersion;

    public WooCommerceAPIMod(OAuthConfig config, ApiVersionType apiVersion) {
//        try {
//
//            final SSLContext sslContext = SSLContext.getInstance("TLS");
//
//            sslContext.init(null, new TrustManager[]{new X509TrustManager() {
//
//                @Override
//                public X509Certificate[] getAcceptedIssuers() {
//                    return new X509Certificate[0];
//                }
//
//                @Override
//                public void checkClientTrusted(X509Certificate[] xcs, String string) throws java.security.cert.CertificateException {
//                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//                }
//
//                @Override
//                public void checkServerTrusted(X509Certificate[] xcs, String string) throws java.security.cert.CertificateException {
//                    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//                }
//            }}, null);
//
//            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
//            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        this.config = config;
        this.client = new DefaultHttpClient();
        this.apiVersion = apiVersion.getValue();
    }

    @Override
    public List<Order> getAllOrders(String endpointBase, Map<String, String> params, String idVendor, String status) {
        List<Order> list = new ArrayList<>();
        if (!idVendor.equals("")) {
            params.put("vendor", idVendor);
        }
        if (!status.equals("")) {
            params.put("status", status);
        }
        params.put("per_page", "100");
        int page = 1;
        try {
            while (true) {
                List<Order> listTmp = null;
                params.remove("page");
                params.put("page", "" + page);
                String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase);
                String signature = OAuthSignature.getAsQueryString(config, url, HttpMethod.GET, params);
                String securedUrl = String.format(URL_SECURED_FORMAT, url, signature);

                HttpClient client2 = HttpClientBuilder.create().build();
                HttpGet request = new HttpGet(securedUrl);
                request.addHeader("accept", "application/json");
                HttpResponse response = client2.execute(request);
                String json = IOUtils.toString(response.getEntity().getContent());

                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                listTmp = mapper.readValue(json, new TypeReference<List<Order>>() {
                });
                for (int i = 0; i < listTmp.size(); i++) {
                    list.add(listTmp.get(i));
                }
                if (listTmp.size() <= 0 || listTmp.size() < 100) {
                    break;
                }
                page++;

//            
                System.out.println("the id is {}");
//            

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public Order getOrder(String endpointBase, Map<String, String> params, String idOrder) {
        Order ord = new Order();

        try {

            
            String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase + "/" + idOrder);
            String signature = OAuthSignature.getAsQueryString(config, url, HttpMethod.GET, params);
            String securedUrl = String.format(URL_SECURED_FORMAT, url, signature);

            HttpClient client2 = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(securedUrl);
            request.addHeader("accept", "application/json");
            HttpResponse response = client2.execute(request);
            String json = IOUtils.toString(response.getEntity().getContent());

            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            ord = mapper.readValue(json, new TypeReference<Order>() {
            });

//            
            System.out.println("the id is {}");
//            

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ord;
    }

    @Override
    public Customer getCustomer(String endpointBase, Map<String, String> params, String idCust) {
        Customer cust = new Customer();

        try {

            List<Customer> listTmp = null;

            String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase + "/" + idCust);
            String signature = OAuthSignature.getAsQueryString(config, url, HttpMethod.GET, params);
            String securedUrl = String.format(URL_SECURED_FORMAT, url, signature);

            HttpClient client2 = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(securedUrl);
            request.addHeader("accept", "application/json");
            HttpResponse response = client2.execute(request);
            String json = IOUtils.toString(response.getEntity().getContent());

            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            cust = mapper.readValue(json, new TypeReference<Customer>() {
            });

//            
            System.out.println("the id is {}");
//            

        } catch (Exception e) {
            e.printStackTrace();
        }
        return cust;
    }

    @Override
    public List<Customer> getAllCustomers(String endpointBase, Map<String, String> params) {
        List<Customer> list = new ArrayList<>();

        params.put("per_page", "100");
        int page = 1;
        try {
            while (true) {
                List<Customer> listTmp = null;
                params.remove("page");
                params.put("page", "" + page);
                String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase);
                String signature = OAuthSignature.getAsQueryString(config, url, HttpMethod.GET, params);
                String securedUrl = String.format(URL_SECURED_FORMAT, url, signature);

                HttpClient client2 = HttpClientBuilder.create().build();
                HttpGet request = new HttpGet(securedUrl);
                request.addHeader("accept", "application/json");
                HttpResponse response = client2.execute(request);
                String json = IOUtils.toString(response.getEntity().getContent());

                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                listTmp = mapper.readValue(json, new TypeReference<List<Customer>>() {
                });
                for (int i = 0; i < listTmp.size(); i++) {
                    list.add(listTmp.get(i));
                }
                if (listTmp.size() <= 0 || listTmp.size() < 100) {
                    break;
                }
                page++;

//            
                System.out.println("the id is {}");
//            

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

//    @Override
//    public List getAllOrders(String endpointBase, Map<String, String> params) {
//        String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase);
////        String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase + "/1/terms");
//        String signature = OAuthSignature.getAsQueryString(config, url, HttpMethod.GET, params);
//        String securedUrl = String.format(URL_SECURED_FORMAT, url, signature);
//
//        try {
//            ArrayList<JsonWcOrdini> lord = new ArrayList<>();
//            HttpClient client2 = HttpClientBuilder.create().build();
//            HttpGet request = new HttpGet(securedUrl);
//            request.addHeader("accept", "application/json");
//            HttpResponse response = client2.execute(request);
//
//            String json = IOUtils.toString(response.getEntity().getContent());
////            String js = "[{\"id\":12,\"date_created\":\"2020-07-14T12:36:59\",\"idcustomer\":15,\"line_items\": ["
////                    + "      {"
////                    + "        \"id\": 315, \"name\": \"Woo Single #1\","
////                    + "        \"product_id\": 93,"
////                    + "        \"variation_id\": 0,"
////                    + "        \"quantity\": 2,"
////                    + "        \"tax_class\": \"\","
////                    + "        \"subtotal\": \"6.00\","
////                    + "        \"subtotal_tax\": \"0.45\","
////                    + "        \"total\": \"6.00\","
////                    + "        \"total_tax\": \"0.45\","
////                    + "        \"taxes\": ["
////                    + "          {"
////                    + "            \"id\": 75,"
////                    + "            \"total\": \"0.45\","
////                    + "            \"subtotal\": \"0.45\""
////                    + "          }"
////                    + "        ],"
////                    + "        \"meta_data\": [],"
////                    + "        \"sku\": \"\","
////                    + "        \"price\": 3"
////                    + "      },"
////                    + "      {"
////                    + "        \"id\": 316,"
////                    + "        \"name\": \"Ship Your Idea &ndash; Color: Black, Size: M Test\","
////                    + "        \"product_id\": 22,"
////                    + "        \"variation_id\": 23,"
////                    + "        \"quantity\": 1,"
////                    + "        \"tax_class\": \"\","
////                    + "        \"subtotal\": \"12.00\","
////                    + "        \"subtotal_tax\": \"0.90\","
////                    + "        \"total\": \"12.00\","
////                    + "        \"total_tax\": \"0.90\","
////                    + "        \"taxes\": ["
////                    + "          {"
////                    + "            \"id\": 75,"
////                    + "            \"total\": \"0.9\","
////                    + "            \"subtotal\": \"0.9\""
////                    + "          }"
////                    + "        ],"
////                    + "        \"meta_data\": ["
////                    + "          {"
////                    + "            \"id\": 2095,"
////                    + "            \"key\": \"pa_color\","
////                    + "            \"value\": \"black\""
////                    + "          },"
////                    + "          {"
////                    + "            \"id\": 2096,"
////                    + "            \"key\": \"size\","
////                    + "            \"value\": \"M Test\""
////                    + "          }"
////                    + "        ],"
////                    + "        \"sku\": \"Bar3\","
////                    + "        \"price\": 12"
////                    + "      }"
////                    + "    ]},{\"id\":13,\"date_created\":\"2020-07-14T12:36:59\",\"idcustomer\":16}]";
//
//            ObjectMapper mapper = new ObjectMapper();
//            TypeReference<Order> listType = new TypeReference<Order>() {
//            };
//            Order list = mapper.readValue(json, listType);
////            TypeReference<List<Order>> listType = new TypeReference<List<Order>>() {
////            };
////            List<Order> list = mapper.readValue(json, listType);
////            JSONArray j = new JSONArray(js);
////            for (int i = 0; i < j.length(); i++) {
////                ObjectMapper objectMapper = new ObjectMapper();
////                JSONObject jso = j.getJSONObject(i);
////                //convert json string to object
////                JsonWcOrdini jo = objectMapper.readValue(jso.toString(), JsonWcOrdini.class);
////
////                lord.add(jo);
////            }
////            JsonWcListaOrdini s = new ObjectMapper().readValue(js, JsonWcListaOrdini.class);
////            Gson g = new Gson();
////            JsonWcListaOrdini p = g.fromJson(json, JsonWcListaOrdini.class);
//
//            System.out.println("the id is {}");
////            for (int i = 0; i < array.length(); i++) {
////                JSONObject object = array.getJSONObject(i);
////                System.out.println("the id is {}" + object.getInt("id"));
////
////            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        return null;
//    }
    @Override
    public List<JsonWcAttribute> getAllAttributes(String endpointBase, Map<String, String> params) {
        List<JsonWcAttribute> list = new ArrayList<>();
        params.put("per_page", "100");
        int page = 1;
        try {
            while (true) {
                List<JsonWcAttribute> listTmp = null;
                params.remove("page");
                params.put("page", "" + page);
                String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase);
                String signature = OAuthSignature.getAsQueryString(config, url, HttpMethod.GET, params);
                String securedUrl = String.format(URL_SECURED_FORMAT, url, signature);
                System.out.println("URL: " + securedUrl);
                HttpClient client2 = HttpClientBuilder.create().build();
                HttpGet request = new HttpGet(securedUrl);
                request.addHeader("accept", "application/json");
                HttpResponse response = client2.execute(request);
                String json = IOUtils.toString(response.getEntity().getContent());

                ObjectMapper mapper = new ObjectMapper();
                listTmp = mapper.readValue(json, new TypeReference<List<JsonWcAttribute>>() {
                });
                for (int i = 0; i < listTmp.size(); i++) {
                    list.add(listTmp.get(i));
                }
                if (listTmp.size() <= 0 || listTmp.size() < 100) {
                    break;
                }
                page++;

//            
                System.out.println("the id is {}");
//            

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List getAllTermsAttribute(String endpointBase, Map<String, String> params, String idAttrib) {
        List<JsonWcAttributeTerms> list = new ArrayList<>();
        params.put("per_page", "100");
        int page = 1;
        try {
            while (true) {
                List<JsonWcAttributeTerms> listTmp = null;
                params.remove("page");
                params.put("page", "" + page);
                String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase + "/" + idAttrib + "/terms");
                String signature = OAuthSignature.getAsQueryString(config, url, HttpMethod.GET, params);
                String securedUrl = String.format(URL_SECURED_FORMAT, url, signature);

                HttpClient client2 = HttpClientBuilder.create().build();
                HttpGet request = new HttpGet(securedUrl);
                request.addHeader("accept", "application/json");
                HttpResponse response = client2.execute(request);
                String json = IOUtils.toString(response.getEntity().getContent());

                ObjectMapper mapper = new ObjectMapper();
                listTmp = mapper.readValue(json, new TypeReference<List<JsonWcAttributeTerms>>() {
                });
                for (int i = 0; i < listTmp.size(); i++) {
                    list.add(listTmp.get(i));
                }
                if (listTmp.size() <= 0 || listTmp.size() < 100) {
                    break;
                }
                page++;

//            
                System.out.println("the id is {}");
//            

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List getAllProductCategories(String endpointBase, Map<String, String> params) {
        List<JsonWcProductCategories> list = new ArrayList<>();
        params.put("per_page", "100");
        int page = 1;
        try {
            while (true) {
                List<JsonWcProductCategories> listTmp = null;
                params.remove("page");
                params.put("page", "" + page);
                String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase);
                String signature = OAuthSignature.getAsQueryString(config, url, HttpMethod.GET, params);
                String securedUrl = String.format(URL_SECURED_FORMAT, url, signature);

                HttpClient client2 = HttpClientBuilder.create().build();
                HttpGet request = new HttpGet(securedUrl);
                request.addHeader("accept", "application/json");
                HttpResponse response = client2.execute(request);
                String json = IOUtils.toString(response.getEntity().getContent());

                ObjectMapper mapper = new ObjectMapper();
                listTmp = mapper.readValue(json, new TypeReference<List<JsonWcProductCategories>>() {
                });
                for (int i = 0; i < listTmp.size(); i++) {
                    list.add(listTmp.get(i));
                }
                if (listTmp.size() <= 0 || listTmp.size() < 100) {
                    break;
                }
                page++;
                System.out.println("the id is {}");
//            

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<JsProdProduct> getAllProducts(String endpointBase, Map<String, String> params, String idVendor) {
        List<JsProdProduct> list = new ArrayList<>();
        if (!idVendor.equals("")) {
            params.put("vendor", idVendor);
        }
        params.put("per_page", "100");
        int page = 1;
        try {
            while (true) {
                List<JsProdProduct> listTmp = null;
                params.remove("page");
                params.put("page", "" + page);
                String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase);
                String signature = OAuthSignature.getAsQueryString(config, url, HttpMethod.GET, params);
                String securedUrl = String.format(URL_SECURED_FORMAT, url, signature);

                HttpClient client2 = HttpClientBuilder.create().build();
                HttpGet request = new HttpGet(securedUrl);
                request.addHeader("accept", "application/json");
                HttpResponse response = client2.execute(request);
                String json = IOUtils.toString(response.getEntity().getContent());

                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                listTmp = mapper.readValue(json, new TypeReference<List<JsProdProduct>>() {
                });
                for (int i = 0; i < listTmp.size(); i++) {
                    list.add(listTmp.get(i));
                }
                if (listTmp.size() <= 0 || listTmp.size() < 100) {
                    break;
                }
                page++;

//            
                System.out.println("the id is {}");
//            

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public List<JsProdVarProduct> getAllProductsVariations(String endpointBase, Map<String, String> params, String idProd) {
        List<JsProdVarProduct> list = new ArrayList<>();
        params.put("per_page", "100");
        int page = 1;
        try {
            while (true) {
                List<JsProdVarProduct> listTmp = null;
                params.remove("page");
                params.put("page", "" + page);
                String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase + "/" + idProd + "/variations");
                String signature = OAuthSignature.getAsQueryString(config, url, HttpMethod.GET, params);
                String securedUrl = String.format(URL_SECURED_FORMAT, url, signature);

                HttpClient client2 = HttpClientBuilder.create().build();
                HttpGet request = new HttpGet(securedUrl);
                request.addHeader("accept", "application/json");
                HttpResponse response = client2.execute(request);
                String json = IOUtils.toString(response.getEntity().getContent());

                ObjectMapper mapper = new ObjectMapper();
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                listTmp = mapper.readValue(json, new TypeReference<List<JsProdVarProduct>>() {
                });
                for (int i = 0; i < listTmp.size(); i++) {
                    list.add(listTmp.get(i));
                }
                if (listTmp.size() <= 0 || listTmp.size() < 100) {
                    break;
                }
                page++;

//            
                System.out.println("the id is {}");
//            

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    @Override
    public JsProdProduct getProduct(String endpointBase, int id) {

        JsProdProduct ris = null;
        try {

            JsProdProduct pr = null;
            String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase + "/" + id);
            String signature = OAuthSignature.getAsQueryString(config, url, HttpMethod.GET);
            String securedUrl = String.format(URL_SECURED_FORMAT, url, signature);

            HttpClient client2 = HttpClientBuilder.create().build();
            HttpGet request = new HttpGet(securedUrl);
            request.addHeader("accept", "application/json");
            HttpResponse response = client2.execute(request);
            String json = IOUtils.toString(response.getEntity().getContent());

            ObjectMapper mapper = new ObjectMapper();
            ris = mapper.readValue(json, new TypeReference<JsProdProduct>() {
            });

//            
            System.out.println("the id is {}");
//            

        } catch (Exception e) {
            e.printStackTrace();
        }
        return ris;
    }

    @Override
    public Map create(String endpointBase, Map<String, Object> object) {
        String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase);
        return client.post(url, OAuthSignature.getAsMap(config, url, HttpMethod.POST), object);
    }

    @Override
    public Map createJson(String endpointBase, Object object) {
        String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase);
        return client.postJson(url, OAuthSignature.getAsMap(config, url, HttpMethod.POST), object);
    }

    @Override
    public Map createJsonTerms(String endpointBase, Object object, String idAttrib) {
        String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase + "/" + idAttrib + "/terms");
        return client.postJson(url, OAuthSignature.getAsMap(config, url, HttpMethod.POST), object);
    }

    @Override
    public Map get(String endpointBase, int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List getAll(String endpointBase, Map<String, String> params) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Map update(String endpointBase, int id, Map<String, Object> object) {
        String url = String.format(API_URL_ONE_ENTITY_FORMAT, config.getUrl(), apiVersion, endpointBase, id);
        return client.put(url, OAuthSignature.getAsMap(config, url, HttpMethod.PUT), object);
    }

    @Override
    public Map updateJson(String endpointBase, int id, Object object) {
        String url = String.format(API_URL_ONE_ENTITY_FORMAT, config.getUrl(), apiVersion, endpointBase, id);
        return client.putJson(url, OAuthSignature.getAsMap(config, url, HttpMethod.PUT), object);
    }

    @Override
    public Map updateJsonTerms(String endpointBase, int id, Object object, String idAttrib) {
        String url = String.format(API_URL_ONE_ENTITY_FORMAT, config.getUrl(), apiVersion, endpointBase + "/" + idAttrib + "/terms", id);
        return client.putJson(url, OAuthSignature.getAsMap(config, url, HttpMethod.PUT), object);
    }

    @Override
    public Map delete(String endpointBase, int id) {
        String url = String.format(API_URL_ONE_ENTITY_FORMAT, config.getUrl(), apiVersion, endpointBase, id);
        return client.delete(url, OAuthSignature.getAsMap(config, url, HttpMethod.DELETE));
    }

    @Override
    public Map batch(String endpointBase, Map<String, Object> object) {
        String url = String.format(API_URL_BATCH_FORMAT, config.getUrl(), apiVersion, endpointBase);
        return client.post(url, OAuthSignature.getAsMap(config, url, HttpMethod.POST), object);
    }

    @Override
    public Map batchJson(String endpointBase, Object object) {
        String url = String.format(API_URL_BATCH_FORMAT, config.getUrl(), apiVersion, endpointBase);
        return client.postJson(url, OAuthSignature.getAsMap(config, url, HttpMethod.POST), object);
    }

}
