package jazzwcapi.woocommerce;

import jazzwcapi.woocommerce.oauth.OAuthSignature;
import jazzwcapi.woocommerce.oauth.OAuthConfig;

import java.util.List;
import java.util.Map;
import jazzwcapi.woocommerce.json.order.Order;
import jazzwcapi.woocommerce.json.product.JsProdProduct;

public class WooCommerceAPI  {
    
}

//public class WooCommerceAPI implements WooCommerce {
//
//    private static final String API_URL_FORMAT = "%s/wp-json/wc/%s/%s";
//    private static final String API_URL_BATCH_FORMAT = "%s/wp-json/wc/%s/%s/batch";
//    private static final String API_URL_ONE_ENTITY_FORMAT = "%s/wp-json/wc/%s/%s/%d";
//    private static final String URL_SECURED_FORMAT = "%s?%s";
//
//    private HttpClient client;
//    private OAuthConfig config;
//    private String apiVersion;
//
//    public WooCommerceAPI(OAuthConfig config, ApiVersionType apiVersion) {
//        this.config = config;
//        this.client = new DefaultHttpClient();
//        this.apiVersion = apiVersion.getValue();
//    }
//
//    @Override
//    public Map create(String endpointBase, Map<String, Object> object) {
//        String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase);
//        return client.post(url, OAuthSignature.getAsMap(config, url, HttpMethod.POST), object);
//    }
//    @Override
//    public Map createJson(String endpointBase, Object object) {
//        String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase);
//        return client.postJson(url, OAuthSignature.getAsMap(config, url, HttpMethod.POST), object);
//    }
//    @Override
//    public Map createJsonTerms(String endpointBase, Object object, String idAttr) {
//        String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase + "/" + idAttr + "/terms");
//        return client.postJson(url, OAuthSignature.getAsMap(config, url, HttpMethod.POST), object);
//    }
//
//    @Override
//    public Map get(String endpointBase, int id) {
//        String url = String.format(API_URL_ONE_ENTITY_FORMAT, config.getUrl(), apiVersion, endpointBase, id);
//        String signature = OAuthSignature.getAsQueryString(config, url, HttpMethod.GET);
//        String securedUrl = String.format(URL_SECURED_FORMAT, url, signature);
//        return client.get(securedUrl);
//    }
//
//    @Override
//    public List getAll(String endpointBase, Map<String, String> params) {
//        String url = String.format(API_URL_FORMAT, config.getUrl(), apiVersion, endpointBase);
//        String signature = OAuthSignature.getAsQueryString(config, url, HttpMethod.GET, params);
//        String securedUrl = String.format(URL_SECURED_FORMAT, url, signature);
//        return client.getAll(securedUrl);
//    }
//
//    @Override
//    public Map update(String endpointBase, int id, Map<String, Object> object) {
//        String url = String.format(API_URL_ONE_ENTITY_FORMAT, config.getUrl(), apiVersion, endpointBase, id);
//        return client.put(url, OAuthSignature.getAsMap(config, url, HttpMethod.PUT), object);
//    }
//    
//    @Override
//    public Map updateJson(String endpointBase, int id, Object object) {
//        String url = String.format(API_URL_ONE_ENTITY_FORMAT, config.getUrl(), apiVersion, endpointBase, id);
//        return client.putJson(url, OAuthSignature.getAsMap(config, url, HttpMethod.PUT), object);
//    }
//    @Override
//    public Map updateJsonTerms(String endpointBase, int id, Object object, String idAttrib) {
//        String url = String.format(API_URL_ONE_ENTITY_FORMAT, config.getUrl(), apiVersion, endpointBase + "/" + idAttrib + "/terms", id);
//        return client.putJson(url, OAuthSignature.getAsMap(config, url, HttpMethod.PUT), object);
//    }
//
//    @Override
//    public Map delete(String endpointBase, int id) {
//        String url = String.format(API_URL_ONE_ENTITY_FORMAT, config.getUrl(), apiVersion, endpointBase, id);
//        Map<String, String> params = OAuthSignature.getAsMap(config, url, HttpMethod.DELETE);
//        return client.delete(url, params);
//    }
//
//    @Override
//    public Map batch(String endpointBase, Map<String, Object> object) {
//        String url = String.format(API_URL_BATCH_FORMAT, config.getUrl(), apiVersion, endpointBase);
//        return client.post(url, OAuthSignature.getAsMap(config, url, HttpMethod.POST), object);
//    }
//    @Override
//    public Map batchJson(String endpointBase, Object object) {
//        String url = String.format(API_URL_BATCH_FORMAT, config.getUrl(), apiVersion, endpointBase);
//        return client.postJson(url, OAuthSignature.getAsMap(config, url, HttpMethod.POST), object);
//    }
//
////    @Override
////    public List getAll2(String endpointBase, Map<String, String> params) {
////        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
////    }
//
//    @Override
//    public List getAllOrders(String endpointBase, Map<String, String> params) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public Order getOrder(String endpointBase, Map<String, String> params) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public List getAll(String endpointBase) {
//        return WooCommerce.super.getAll(endpointBase); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public List getAllOrders(String endpointBase) {
//        return WooCommerce.super.getAllOrders(endpointBase); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public List getAllAttributes(String endpointBase) {
//        return WooCommerce.super.getAllAttributes(endpointBase); //To change body of generated methods, choose Tools | Templates.
//    }
//    @Override
//    public List getAllProducts(String endpointBase) {
//        return WooCommerce.super.getAllProducts(endpointBase); //To change body of generated methods, choose Tools | Templates.
//    }
//    
//    
//    
//
//    @Override
//    public Order getOrder(String endpointBase) {
//        return WooCommerce.super.getOrder(endpointBase); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public List getAllAttributes(String endpointBase, Map<String, String> params) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public List getAllTermsAttribute(String endpointBase, Map<String, String> params, String idAttr) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//    @Override
//    public List getAllProductCategories(String endpointBase, Map<String, String> params) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//    @Override
//    public List getAllProducts(String endpointBase, Map<String, String> params) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//    @Override
//    public List getAllProductsVariations(String endpointBase, Map<String, String> params, String idProd) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//    @Override
//    public JsProdProduct getProduct(String endpointBase, int id) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//}
