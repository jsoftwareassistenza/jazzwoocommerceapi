package jazzwcapi.woocommerce;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import jazzwcapi.woocommerce.json.customer.Customer;
import jazzwcapi.woocommerce.json.order.Order;
import jazzwcapi.woocommerce.json.product.JsProdProduct;

/**
 * Main interface for WooCommerce REST API
 */
public interface WooCommerce {

    /**
     * Creates WooCommerce entity
     *
     * @param endpointBase API endpoint base @see EndpointBaseType
     * @param object       Map with entity properties and values
     * @return Map with created entity
     */
    Map create(String endpointBase, Map<String, Object> object);
    Map createJson(String endpointBase, Object object);
    Map createJsonTerms(String endpointBase, Object object, String idAttr);

    /**
     * Retrieves on WooCommerce entity
     *
     * @param endpointBase API endpoint base @see EndpointBaseType
     * @param id           id of WooCommerce entity
     * @return Retrieved WooCommerce entity
     */
    Map get(String endpointBase, int id);

    /**
     * Retrieves all WooCommerce entities with request parameters
     *
     * @param endpointBase API endpoint base @see EndpointBaseType
     * @param params additional request params
     * @return List of retrieved entities
     */
    List getAll(String endpointBase, Map<String, String> params);
    
    List getAllOrders(String endpointBase, Map<String, String> params, String idVendor, String status);
    Order getOrder(String endpointBase, Map<String, String> params, String idOrder);
    List getAllCustomers(String endpointBase, Map<String, String> params);
    Customer getCustomer(String endpointBase, Map<String, String> params, String idCust);
    List getAllAttributes(String endpointBase, Map<String, String> params);
    List getAllTermsAttribute(String endpointBase, Map<String, String> params, String idAttr);
    List getAllProductCategories(String endpointBase, Map<String, String> params);
    List getAllProducts(String endpointBase, Map<String, String> params, String idVendor);
    List getAllProductsVariations(String endpointBase, Map<String, String> params, String idProd);
    JsProdProduct getProduct(String endpointBase, int id);
    
    

    /**
     * Retrieves all WooCommerce entities
     *
     * @param endpointBase API endpoint base @see EndpointBaseType
     * @return List of retrieved entities
     */
//    default List getAll(String endpointBase) {
//        return getAll(endpointBase, Collections.emptyMap());
//    }
//    default List getAllOrders(String endpointBase) {
//        return getAllOrders(endpointBase, Collections.emptyMap());
//    }
//    default List getAllAttributes(String endpointBase) {
//        return getAllAttributes(endpointBase, Collections.emptyMap());
//    }
//    default List getAllProducts(String endpointBase) {
//        return getAllProducts(endpointBase, Collections.emptyMap());
//    }
//    
//    default Order getOrder(String endpointBase) {
//        return getOrder(endpointBase, Collections.emptyMap());
//    }

    /**
     * Updates WooCommerce entity
     *
     * @param endpointBase API endpoint base @see EndpointBaseType
     * @param id           id of the entity to update
     * @param object       Map with updated properties
     * @return updated WooCommerce entity
     */
    Map update(String endpointBase, int id, Map<String, Object> object);
    
    Map updateJson(String endpointBase, int id, Object object);
    
    Map updateJsonTerms(String endpointBase, int id, Object object, String idAttrib);

    /**
     * Deletes WooCommerce entity
     *
     * @param endpointBase API endpoint base @see EndpointBaseType
     * @param id           id of the entity to update
     * @return deleted WooCommerce entity
     */
    Map delete(String endpointBase, int id);

    /**
     * Makes batch operations on WooCommerce entities
     *
     * @param endpointBase API endpoint base @see EndpointBaseType
     * @param object       Map with lists of entities
     * @return response Map with WooCommerce entities implicated
     */
    Map batch(String endpointBase, Map<String, Object> object);
    Map batchJson(String endpointBase, Object object);

}
